<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/modeles_media.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'media_description' => 'Les modèles &lt;doc&gt;, &lt;img&gt; et &lt;emb&gt; produisent chacun un résultat différent et ce résultat, pour les images, dépend du fait qu’elle soit dans le portfolio ou non. Ce plugin propose une nouvelle série de modèles ayant un comportement unifié et indépendant du mode des images. Les modèles existants (doc, emb, img) ne sont pas modifiés afin d’assurer la rétrocompatibilité.',
	'media_slogan' => 'Modèles alternatifs pour l’insertion des documents'
);
