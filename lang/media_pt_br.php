<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/media?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'credits' => 'Créditos:',

	// E
	'erreur_saisies' => 'Os dados informados contêm erros!',
	'erreur_taille' => 'Você deve informar um número inteiro positivo.',
	'explication_alt' => 'Você pode personalizar o texto alternativo exibido se o navegador não puder exibir a mídia (para as imagens, trata-se do atributo <i>alt</i>). Deixe em branco para que o textgo alternativo seja gerado automaticamente.',
	'explication_configurer_media' => 'Você pode definir quatro tamanhos padrão usáveis com os modelos &lt;media&gt; via o parâmetro <i>|taille</i>.',
	'explication_descriptif' => 'Os atalhos tipográficos do SPIP podem ser usados.',
	'explication_lien' => 'Você pode informar um URL no formato http://meusite.net, um endereço de e-mail ou um atalho do SPIP (rub1, art3, breve4...).',
	'explication_titre_lien' => 'Você pode ppersonalizar o atributo <i>title</i> do link. Deixe em branco para queo atributo <i>title</i> seja gerado automaticamente.',
	'extension' => 'Extensão:',

	// H
	'hauteur' => 'Altura:',

	// I
	'info_configurer_media_titre' => 'Modelos &lt;media&gt;: configurar os tamanhos padrão',
	'info_inclusion_icone' => 'Inclusão do ícone:',
	'item_afficher' => 'exibir',
	'item_icone' => 'exibir o ícone',
	'item_insert' => 'incluir o documento',
	'item_legende_complete' => 'legenda completa',
	'item_legende_simple' => 'legenda simples (título + descrição)',
	'item_lien_document' => 'link no documento',
	'item_ne_pas_afficher' => 'não exibir',
	'item_ne_pas_redimensionner' => 'não redimensionar',
	'item_pas_de_lien' => 'sem link',
	'item_personnaliser' => 'personalisar',
	'item_taille_grand' => 'grande',
	'item_taille_icone' => 'ícone',
	'item_taille_moyen' => 'médio',
	'item_taille_petit' => 'pequeno',
	'item_vignette' => 'exibir a vinheta',

	// L
	'label_alt' => 'Texto alternativo:',
	'label_configurer_media_largeur_legende' => 'Largura das legendas:',
	'label_configurer_media_taille_defaut' => 'Tamanho padrão (opcional):',
	'label_configurer_media_taille_grand' => 'Tamanho ’grande’:',
	'label_configurer_media_taille_icone' => 'Tamanho ’ícone’:',
	'label_configurer_media_taille_moyen' => 'Tamanho ’médio’:',
	'label_configurer_media_taille_petit' => 'Tamanho ’pequeno’:',
	'label_credits' => 'Créditos:',
	'label_descriptif' => 'Descrição:',
	'label_hauteur' => 'Altura (em pixels):',
	'label_id_document' => 'Documento número:',
	'label_largeur' => 'Largura (em pixels):',
	'label_legende' => 'Legenda:',
	'label_legende_personnalisee' => 'Legenda personalisada',
	'label_lien' => 'Link:',
	'label_poids' => 'Peso (em bites) :',
	'label_taille' => 'Tamanho:',
	'label_taille_personnalisee' => 'Tamanho personalisado',
	'label_titre' => 'Título:',
	'label_titre_lien' => 'Título do link:',
	'label_type' => 'Tipo de documento:',
	'label_variante' => 'O que você deseja?',
	'largeur' => 'Largura:',
	'legend_autres' => 'Outros parâmetros',
	'legend_tailles' => 'Tamanhos padrão',

	// M
	'maj' => 'Atualizado em:',
	'mime_type' => 'Tipo MIME:',
	'modeles_media' => 'Modelos &lt;media&gt;',

	// N
	'nom_media' => 'um documento',

	// P
	'pixels_hauteur' => 'pixels de altura',
	'pixels_largeur' => 'pixels de largura',
	'pixels_maximum' => 'pixels no máximo',
	'pixels_minimum' => 'pixels no mínimo'
);
