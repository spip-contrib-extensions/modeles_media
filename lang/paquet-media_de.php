<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-media?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'media_description' => 'Die Modelle <doc>, <img> et <emb> produzieren jeweils unterschiedliche Darstellungen eines Dokuments und die von Bildern hängt zusätzlich davon ab, ob sich dieses im Portfolio befindet oder nicht. Dieses Plugin enthält eine Serie neuer Modelle, die eine einheitliche Darstellung der Bilder realisieren, und zwar unabhängig von ihrem "Bildmodus". 
Die verhandenen Modelle (doc, emb, img) werden nicht modifiziert, damit die Abwärtskompatibilität gesichert bleibt..', # MODIF
	'media_slogan' => 'Alternative Modelle zum EInbetten von Dokumenten'
);
