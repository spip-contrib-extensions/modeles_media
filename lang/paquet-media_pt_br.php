<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-media?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'media_description' => 'Os modelos <doc>, <img> e <emb> produzem, cada um, resultados diferentes e esses resultados para as imagens dependem delas estarem ou não num portfolio. Este plugin propõe uma nova série de modelos com um comportamento unificado e independente do modo das imagens. Os modelos existentes (doc, emb, img) não são alterados para garantir a compatibilidade retroativa.',
	'media_slogan' => 'Modelos alternativos para a inserção de documentos'
);
